function [ X,Y,Z,Heading,Roll,T ] = generateAllPathsForState(vX,vZ,roll,rollResolution,rollMaxAngle,rollRate,decceleration,maxDeccZ,deccZResolution,vMin,reactionTime,obstacleSize)
X = [];
Y = [];
Z = [];
Heading = [];
Roll = [];
T = [];

for i=-rollMaxAngle:rollResolution:rollMaxAngle
    for j = -maxDeccZ:deccZResolution:maxDeccZ
        [x,y,z,theta,r,t] = generateChangingCurvatureTrajectory3(vX,vZ,roll,i,rollRate,decceleration,j,vMin,reactionTime,obstacleSize);
        X = [X;x];
        Y = [Y;y];
        Z = [Z;z];
        Heading = [Heading;theta];
        Roll = [Roll;r];
        T = [T;t];        
        if(size(X,1)~= size(T,1))
            display('here is the error')
        end
    end
end



% now there will be 2 things 2 vary roll and zdecc